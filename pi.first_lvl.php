<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$plugin_info = array (
    'pi_name'           => 'First Level',
    'pi_version'        => '2.1',
    'pi_author'         => 'Eric Slenk',
    'pi_description'    => "Returns an entry's first-level parent.",
    'pi_usage'          => first_lvl::usage()
);

/**
 * Get First Level Class
 *
 * @package ExpressionEngine
 * @category Plugin
 * @author Eric Slenk
 */
class First_lvl {
	var $site_id; /** ID number of the current site */
	var $debug; /** Boolean determining whether the plugin shows debugging output. */

    /**
     * Constructor for first_lvl object.
     *
     * Gets ExpressionEngine instance.
     *
     * @access public
     */
    public function __construct ()
    {
		$this->EE =& get_instance();
    
		$this->debug = ( $this->EE->TMPL->fetch_param('debug') == "yes" ) ?
			TRUE :
			FALSE;
		
		$this->site_id = ( isset($this->EE->config->_global_vars['site_id']) ) ?
			$this->EE->config->_global_vars['site_id'] :
			FALSE;
		if ( !$this->site_id && $this->debug )
			die("Unable to fetch site_id.");
		
    }

    /**
     * Returns the first-level parent's entry_id.
     *
     * Public-facing wrapper for get_top_level_id().
     *
     * @access public
     * @return int Integer representing the top-level page's entry_id.
     */
    public function id ()
    {
        return $this->get_top_lvl_id();
    }

    /**
     * Returns the first-level parent's url_title.
     *
     * Queries exp_channel_titles for the url_title of the first-level parent
     * using its entry_id.
     *
     * @access public
     * @return string String representing the top-level page's url_title.
     */
    public function url_title ()
    {
        $entry_id = $this->get_top_lvl_id();
        if (!$entry_id)
            return FALSE;

        $url_title = reset($this->EE->db
            -> select ('url_title')
            -> from ('exp_channel_titles')
            -> where ('entry_id', $entry_id)
            -> get() -> result())
            -> url_title;

        return $url_title;
    }

    /**
     * Returns the first-level parent's structure URI.
     *
     * @access public
     * @return string Returns a Structure-style URI string.
     */
    public function structure_uri ()
    {
    	// Get ID of top-level entry
        $entry_id = $this->get_top_lvl_id();
        if ( !$entry_id )
        	return FALSE;
    	
    	// Get Structure URIs
    	$structure_pages = $this->get_structure_pages();
    	if ( !$structure_pages )
    		return FALSE;
    	
    	// Return URI for top-level entry
    	return $structure_pages[ $this->site_id ]['uris'][ $entry_id ];
    }
    
    
    
    /**
     * Gets the entry ID for the top-level page.
     *
     * Starting at the current page, finds the parent of the current entry
     * until an entry is found which has no parent and retuns this entry's ID.
     *
     * @access private
     * @return mixed Returns an integer representing the top-level page's entry ID on success, FALSE on failure.
     */
    private function get_top_lvl_id ()
    {
        // Find the column to query for parent IDs
        $parent_column = $this->get_column_id('parent_page');
        if ( !$parent_column )
        	return FALSE;

        // Initialize the current entry to be the current page's entry ID
        $current = $this->get_current_id();
        if ( !$current )
        	return FALSE;

        // Loop until an entry without a parent is found
        for (;;)
        {
			// Get parent entry
			$parent = $this->get_parent_id($current, $parent_column);
			
			// Current entry is top if get_parent_id == FALSE
			if (!$parent)
			{
				return $current;
			}
			
			// Get next level parent
			else
			{
				$current = $parent;
			}
        }
	}
	
    /**
     * Gets the ID for the specified column.
     *
     * Runs a query on exp_channel_fields to fetch the field ID number
     * for the given field.
     *
     * @access private
     * @param string $field_name String containing the name of the field to
     * @return mixed Returns a string containing the header of the Parent Page column on success, FALSE on failure.
     */
    private function get_column_id ($field_name)
    {
		$field_id = $this->EE->db
            -> select ('field_id')
            -> from ('exp_channel_fields')
            -> where ( 'field_name', $field_name )
            -> get() -> result_array();
		
		if ( empty($field_id) )
		{
			if ( $this->debug )
				die("Unable to find field_id for field '$field_name' on site $this->site_id.");
			else
				return FALSE;
		}
			
        return $field_id[0]['field_id'];
    }

    /**
     * Gets the entry ID of the current page
     *
     * @access private
     * @return mixed Returns the integer ID of the current entry on success, FALSE on failure.
     */
    private function get_current_id ()
    {
        // Get URL title
        $num_segs = $this->EE->uri->total_segments();
        $url_title = ($num_segs > 0) ?
            $this->EE->uri->segment($num_segs) : # last segment
            "home";
        
        // Get channel IDs
        $channel_ids = $this->get_channel_ids( $this->get_channel_names() );

        // Get Entry ID
        $entry_id = $this->EE->db
            -> select ('entry_id')
            -> from ('exp_channel_titles')
            -> where ( 'site_id', $this->site_id )
			-> where_in ('channel_id', $channel_ids )
            -> where ('url_title', $url_title)
            -> get () -> result_array();

        if ( empty($entry_id) )
        {
        	if ($this->debug)
	        	die("Unable to fetch current entry's entry_id.");
        	else
        		return FALSE;
        }
        
        return $entry_id[0]['entry_id'];
    }

    /**
     * Gets the names of the search channels.
     *
     * @access private
     * @return array Returns an array of the names of the channels to search through.
     */
    private function get_channel_names ()
    {
        return explode( ',', $this->EE->TMPL->fetch_param('channels') );
    }
    
    /**
     * Gets the IDs of the search channels.
     *
     * @access private
     * @param array $channel_names Array of strings which are the names of the channels to search.
     * @return array Returns an array of integers which are the IDs of the channels to search.
     */
    private function get_channel_ids ( $channel_names )
    {
        $channel_ids = array();
		$temp = $this->EE->db
			-> select ('channel_id')
			-> from ('exp_channels')
			-> where ( 'site_id', $this->site_id )
			-> where_in ('channel_name', $channel_names)
			-> get() -> result_array();
        foreach ($temp as $row)
        {
           $channel_ids[] = $row['channel_id'];
        }
        
        return $channel_ids;
    }
    
    /**
     * Gets the parent page of an entry.
     *
     * Runs a query on exp_channel_data to find the child entry's row, then
     * returns the contents of the predetermined parent ID column.
     *
     * @access private
     * @param int $child_id ID number of the child entry.
     * @param int $parent_column_id Field ID for the column to pull parent IDs
     * from.
     * @return mixed Integer representing the parent entry's entry ID if one exists. Otherwise, returns FALSE.
     */
    private function get_parent_id ($child_id, $parent_column_id)
    {
        // Get the ID as the only member of an array
        $rel_id_query = reset($this->EE->db
            -> select ('field_id_'.$parent_column_id)
            -> from ('exp_channel_data')
            -> where ('entry_id', $child_id)
            -> get() -> result());

        // False if the given ID has no parent page
        if (!$rel_id_query || !current($rel_id_query))
        {
            return FALSE;
        }
        else
        {
            $rel_id = current($rel_id_query);
        }

        // Find and return the parent ID
		$parent_id = reset($this->EE->db
            -> select ('rel_child_id') # Not quite sure why selecting
                                       # rel_child_id gets the parent ID,
                                       # but it does, and using
                                       # rel_parent_id seems to spit out the
                                       # child ID.
			-> from ('exp_relationships')
			-> where ('rel_id', $rel_id)
			-> get() -> result())
            -> rel_child_id;

        return $parent_id;
    }
    
    /**
     * Gets the unserialized Structure page tree.
     *
     * @access private
     * @return mixed Returns an array representing the Structure page tree on success, FALSE on failure.
     */
    private function get_structure_pages ()
    {
    	$pages_serialized = $this->EE->db
    		-> select ('site_pages')
    		-> from ('exp_sites')
    		-> where ( 'site_id', $this->site_id )
    		-> get() -> result_array();
    	
    	if ( empty($pages_serialized) )
    	{
    		if ($this->debug)
	    		die ("Unable to load Structure page tree for site $this->site_id.");
    		else
    			return FALSE;
    	}
    	
    	
    	return unserialize( base64_decode( $pages_serialized[0]['site_pages'] ) );
    }

    /**
     * Describes how the plugin should be used.
     *
     * @access public
     * @return string String containing text which describes how to use the plugin.
     */
    public static function usage ()
    {
        ob_start();
?>
The First Level Plugin identifies the parent of the current page and
outputs the parent's identity based on the third tag segment.

USAGE

    {exp:fist_lvl:[seg_3] channels="[channel1][,[channel2]...,[channelN]]" [OPTIONAL PARAMETERS]}

SEGMENT 3

    id : Plugin returns the entry_id of the first-level parent.
    url_title : Plugin returns the url_title of the first-level parent.
    structure_uri : Plugin returns the structure URI of the first-level parent.

PARAMETERS
    
    REQUIRED
    
	    channels : String containing a comma-separated list of channel names to search for parents in.
        
    OPTIONAL
    
	    debug : Boolean determining whether debugging mode is on. If "yes", errors will cause the plugin to die with a status message. Default is "no".
    
<?php
        $buffer = ob_get_contents();
        ob_end_clean();

        return $buffer;
    }
}

/**
 * Pretty print an object.
 *
 * @param object $obj Data object to be passed through print_r()
 */
function display ($obj) {
    echo "<pre>";
    print_r($obj);
    echo "</pre>";
}

/* End of file pi.first_lvl.php */
/* Location: ./system/expressionengine/third_party/first_lvl/pi.first_lvl.php */
